<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Plot_Creative_Parent_Theme
 */

?>
<!doctype html>

<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link href="https://fonts.googleapis.com/css?family=Work+Sans:500,600,700&display=swap" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'plot' ); ?></a>

	<header id="site-header">
		<div class="header-inner">

			<div class="header-top">

				<div class="logo">
					<?php the_custom_logo(); ?>
				</div>

				<div class="mobile-controls">
					<button id="menu-btn" aria-controls="primary-menu" aria-expanded="false"  uk-toggle="target: #site-header; cls: active">
						<span class="bar-icon"></span>
						<span class="bar-icon"></span>
						<span class="bar-icon"></span>
					</button>
				</div>

			</div><!-- .header-top -->

			<nav id="site-navigation">
				<?php wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'walker'			=> new Walker_UIKIT()
				) ); ?>
			</nav>

		</div>

	</header><!-- #site-header -->

<div id="site-content">