
let Navbar = function () {

	// These are available in all functions
	let header = document.getElementById("site-header");
	let menu_btn = document.getElementById("menu-btn");


	// run me to to begin
	this.init = function () {

		bind_events();

	}; // init()


	// Close the menu by remove class
	let close_nav = function () {

		header.removeAttribute("class");

	}; // close_nav()


	// Open / close the menu
	let toggle_nav = function () {

		header.classList.toggle("active");

	}; // toggle_nav()



	// React to mouse/touch and window resizes
	let bind_events = function () {
		menu_btn.addEventListener('click', toggle_nav);
		window.addEventListener('resize', close_nav);
	};

}; // Ictero

  
// Run when page is loaded
document.addEventListener("DOMContentLoaded", function (event) {
  
	let nav = new Navbar();
	nav.init();

});
